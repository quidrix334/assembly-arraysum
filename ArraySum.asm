#include "p16f877.inc"

		sum equ 30h
		startf equ 20h
		endf equ 25h
	
		Org 00
		;Memory init
		bsf STATUS,RP0
		movlw b'00000000'
		movwf TRISB
		bcf STATUS,RP0
		movlw 00
		movwf sum
		movlw d'1'
		movwf 20h
		movlw d'2'
		movwf 21h
		movlw d'3'
		movwf 22h
		movlw d'4'
		movwf 23h
		movlw d'5'
		movwf 24h
		movlw d'6'
		movwf 25h
		movlw d'7'
		movwf 26h
		movlw d'8'
		movwf 27h
		movlw d'9'
		movwf 28h
		movlw d'10'
		movwf 29h

main 	movlw startf
		movwf FSR
loop	movf INDF,w
		addwf sum,1
		movf FSR,w
		sublw endf
		btsfc STATUS,Z
		goto finish
		incf FSR,1
		goto loop
finish 	movf sum,w
		movwf PORTB
label	goto label
		end